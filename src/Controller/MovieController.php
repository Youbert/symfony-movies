<?php

namespace App\Controller;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    /**
     * @Route("/movie/{slug}", name="movie_details")
     * @param Movie $movie
     * @return Response
     */
    public function details(Movie $movie)
    {
        return $this->render('movie/details.html.twig', [
            'movie' =>  $movie,
        ]);
    }
}