<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtistRepository")
 */
class Artist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $usageName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biography;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movie", inversedBy="directors")
     */
    private $moviesMade;

    public function __construct()
    {
        $this->moviesMade = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUsageName(): ?string
    {
        return $this->usageName;
    }

    public function setUsageName(string $usageName): self
    {
        $this->usageName = $usageName;

        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): self
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMoviesMade(): Collection
    {
        return $this->moviesMade;
    }

    public function addMoviesMade(Movie $moviesMade): self
    {
        if (!$this->moviesMade->contains($moviesMade)) {
            $this->moviesMade[] = $moviesMade;
        }

        return $this;
    }

    public function removeMoviesMade(Movie $moviesMade): self
    {
        if ($this->moviesMade->contains($moviesMade)) {
            $this->moviesMade->removeElement($moviesMade);
        }

        return $this;
    }
}
