<?php

namespace App\DataFixtures;

use App\Entity\Artist;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;


class ArtistFixtures extends Fixture
{
    const COUNT = 200;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
       for($i = 0; $i <self::COUNT; $i++){
           $artist = new Artist();
           $artist->setFirstName($faker->firstName);
           $artist->setLastName($faker->lastName);
           $artist->setUsageName($faker->userName);
           $artist->setBiography($faker->text);
           $manager->persist($artist);
           $this->addReference('artist'.$i, $artist);
       }

        $manager->flush();
    }
}
