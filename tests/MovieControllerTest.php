<?php

namespace App\Tests;

use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Panther\PantherTestCase;

class MovieControllerTest extends WebTestCase
{

    public function testMovieDetailsIsBrowsableFromHome()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $link = $crawler->filter('#last_movies li a')->link();
        $crawlerDetails = $client->click($link);
        $container = self::$container;
        /** @var MovieRepository $movieRepository */
        $movieRepository = $container->get(MovieRepository::class);
        $lastMovie = $movieRepository->findLastMovie();
        $this->assertContains($lastMovie->getTitle(), $crawlerDetails->filter('h1')->text());
        $this->assertContains($lastMovie->getDate()->format('d/m/Y'), $crawlerDetails->filter('time')->text());
        $this->assertGreaterThanOrEqual(1, $lastMovie->getDirectors()->count());
        foreach ($lastMovie->getDirectors() as $director){
            $this->assertContains($director->getUsageName(), $crawlerDetails->filter('time ~ p')->text());
        }


    }
}
