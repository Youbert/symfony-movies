<?php

namespace App\Tests;

use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testHomePageIsReachable()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Symfony Movies", $crawler->filter('h1')->text());
    }

    public function testLastMovieList(){
        $client = static::createClient();
        $container = self::$container;

        $movieRepository = $container->get(MovieRepository::class);

        $movies = $movieRepository->findLastMovies();

        $crawler = $client->request('GET', '/');
        $lastMovies = $crawler->filter('#last_movies');
        $this->assertSame(3, $lastMovies->filter('li')->count() );
        foreach ($movies as $movie){
            $this->assertContains($movie->getTitle(), $lastMovies->text());
        }
    }
}